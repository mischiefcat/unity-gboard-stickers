﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MischiefCat.Stickers
{
	/// <summary>
	/// Class used to install the Android GBoard Stickers
	/// </summary>
	public static class MischiefCatGBoardStickerInstaller
	{
		/// <summary>
		/// Call this method to install the stickers
		/// </summary>
		public static void InstallStickers()
		{
			try
			{
				using (AndroidJavaClass stickerInstallerClass = new AndroidJavaClass("com.mischiefcat.mischiefcatgboardstickers.VulcanStickerAppIndexingService"))
				{
					stickerInstallerClass.CallStatic("EnqueueStickerIndexingFromXMLFile");
				}
			}
			catch (System.Exception ex)
			{
				UnityEngine.Debug.LogException(ex);
			}
		}

		/// <summary>
		/// Installs the stickers with the given JSON string.
		/// </summary>
		/// <param name="argStickerPackJSON">JSON string representation of the sticker pack we want to install.</param>
		public static void InstallStickersWithJSON(string argStickerPackJSON)
		{
			try
			{
				using (AndroidJavaClass stickerInstallerClass = new AndroidJavaClass("com.mischiefcat.mischiefcatgboardstickers.VulcanStickerAppIndexingService"))
				{
					stickerInstallerClass.CallStatic("EnqueueStickerIndexing", argStickerPackJSON);
				}
			}
			catch (System.Exception ex)
			{
				UnityEngine.Debug.LogException(ex);
			}
		}
	}
}