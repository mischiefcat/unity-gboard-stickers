Mischief Cat GBoard Stickers
===================

This plugin allows you to install GBoard stickers for your Unity application. These are similar to iMessage stickers.

https://android-developers.googleblog.com/2017/09/create-stickers-for-gboard-on-google.html

Prerequisites
--------------
Before beginning, you'll want to add your game as Firebase project. You can find steps on how to do so here:
https://firebase.google.com/docs/unity/setup

Note that you don't need to import any of the Firebase packages.

Setup
--------------
In order to make this plugin work you must:
1) Add all of your image assets into the MischiefCatGBoardStickers/Plugins/Android/MischiefCatGBoardStickerAssets/res/raw folder.

2) Update the defaultstickerpack.xml file inside of MischiefCatGBoardStickers/Plugins/Android/MischiefCatGBoardStickerAssets/res/xml.
The image URLs can either be embedded links, like in the example, or have download URLs. If you use downloadable URLS, you do not need to embedd the images into the res/raw folder.

3) Add in your google-services.xml file to the MischiefCatGBoardStickers/Plugins/Android/MischiefCatGBoardStickerAssets/res/VALUES FOLDER.
This allows us to build the project with Firebase indexing without having to apply the "com.google.gms.google-services" plugin and have the google-services.json file.

The Firebase Unity package includes a python script that will automatically conver the .json file into the needed .xml file (https://firebase.google.com/docs/unity/setup).

4) You must add the Android library dependencies. If your project includes the Unity jar resolver, this will be handle automatically (https://github.com/googlesamples/unity-jar-resolver).
If not, you must be sure to add:
'com.google.firebase:firebase-appindexing:16.0.2'
'com.google.code.gson:gson:2.8.5'
'com.android.support:support-v4:28.0.0'

5) Your build target must be set to at least SDK 28

6) To install your stickers, call the "InstallStickers()" method in MischiefCatGBoardStickerInstaller.cs.

Android Library

If you need to rebuild or extend the Android .aar library, the repository [can be found here](https://bitbucket.org/mischiefcat/gboard-stickers-library/src/master/).

License
-------
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Copyright 2018 Mischief Cat, LLC

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
